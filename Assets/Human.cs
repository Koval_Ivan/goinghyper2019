﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Human : MonoBehaviour
{
    public HumanState CurrentHumanState;

    public bool FollowTarget = true;
    private float SmoothTime = 0.3f;
    public bool IsInSubway = false;
    public Vector3 InitialOffset;
    Vector3 velocity = Vector3.zero;
    [SerializeField] public float bounds = 4.2f;
    [SerializeField] float shift = 1f;
    [SerializeField] float baseAnimSpeed = 1f;
    [SerializeField] float levelSpeed = 6f;
    [SerializeField] Animator legAnimatior;
    [SerializeField] MeshRenderer[] screenMRs;
    [SerializeField] MeshRenderer[] skinMRs;
    [SerializeField][ColorUsage(true,true)] Color screenColor;
    [SerializeField][ColorUsage(true,true)] Color skinColor;
    [SerializeField] float startSpeed = 2f;

    public GameObject plusOneText;

    private IDisposable _followTargetDisposable;

    private void Start()
    {
        SmoothTime = Random.Range(0.2f, 0.5f); //0.3; 0.75

        if (plusOneText != null)
        {
            this.UpdateAsObservable()
                .Subscribe(x =>
                {
                    var targetRotation = transform.InverseTransformDirection(Vector3.zero);
                    targetRotation.x = -75;

                    plusOneText.transform.rotation = Quaternion.Euler(targetRotation);
                });
        }

        if (CurrentHumanState == HumanState.Recruit)
        {
            GetComponent<Collider2D>().isTrigger = true;
        }

        this.OnTriggerEnter2DAsObservable()
            .Where(col => CurrentHumanState.Equals(HumanState.Follower))
            .Subscribe(col =>
            {
                if (col.tag.Equals(Constants.Tags.Human))
                {
                    var human = col.gameObject.GetComponent<Human>();
                    if (human.CurrentHumanState.Equals(HumanState.Recruit))
                    {
                        human.InitialOffset = GroupController.Instance.GetOffset(transform.position);
                        human.InitAsFollower(true);
                    }
                }
                else if (col.tag.Equals(Constants.Tags.Obstacle))
                {
                    BetrayGroup();

                    foreach (MeshRenderer mr in screenMRs)
                    {
                        mr.material.SetColor("_Color", Color.black);
                    }

                    //Observable.Timer(TimeSpan.FromSeconds(15f))
                    //    .Subscribe(x =>
                    //    {
                    //        if(gameObject != null)
                    //            Destroy(gameObject);
                    //    });
                }
                else if (col.tag.Equals(Constants.Tags.ObstacleCar))
                {
                    BetrayGroup();

                    var carDirection = col.gameObject.transform.up;
                    carDirection.x *= 7f;
                    carDirection.z += -5f;
                    carDirection.y = 3f;

                    var currentPos = transform.position;
                    currentPos.x = 0;
                    

                    var targetPos = currentPos + carDirection;
                    targetPos += Random.insideUnitSphere * 3f;

                    transform.DOScale(Vector3.zero, 1f).SetEase(Ease.InCirc);
                    transform.DOMoveX(targetPos.x, 1f).SetEase(Ease.Linear);
                    transform.DOMoveY(targetPos.y, 1f).SetRelative(true);
                    transform.DOMoveZ(targetPos.z, 1f).SetEase(Ease.OutQuad)
                        .OnComplete(() =>
                        {
                            Destroy(gameObject);
                        });

                    transform.DORotate(Random.insideUnitSphere * 180, 1.5f);

                }
            });
    }

    private void Update()
    {
        if (legAnimatior)
        {
            if (velocity == Vector3.zero) legAnimatior.speed = 0f;
            else legAnimatior.speed = (velocity + Vector3.up * levelSpeed).magnitude * baseAnimSpeed;
        }
    }

    public void InitAsFollower(bool vibrate = false)
    {
        GetComponent<Collider2D>().isTrigger = false;
        GetComponent<CircleCollider2D>().radius = 0.5f;
        CurrentHumanState = HumanState.Follower;
        foreach (MeshRenderer mr in screenMRs)
        {
            mr.material.SetColor("_Color", screenColor);
        }
        foreach (MeshRenderer mr in screenMRs)
        {
            mr.material.SetColor("_Skin", skinColor);
        }
        velocity = transform.up * startSpeed + Vector3.down * levelSpeed;
        transform.DORotate(Vector3.zero, 1f, RotateMode.Fast);
        
        _followTargetDisposable = this.FixedUpdateAsObservable()
            .Where(x => FollowTarget)
            .Subscribe(x =>
            {
                var newOffset = InitialOffset + (Vector3)Random.insideUnitCircle * shift * Time.fixedDeltaTime;
                if (newOffset.magnitude > bounds) newOffset = newOffset.normalized * bounds;
                InitialOffset = newOffset;
                Vector3 targetPosition = GroupController.Instance.FollowTarget.transform.TransformPoint(InitialOffset);
                targetPosition.z = 0f;
                targetPosition.x = Mathf.Clamp(targetPosition.x, -4.2f, 4.2f);
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);
            });

        GroupController.Instance.HumanList.Add(this);
        GroupController.Instance.RecalculateBounds();

        transform.parent = GroupController.Instance.gameObject.transform;

        if (vibrate)
        {
            Vibration.Vibrate(20);

            if (plusOneText == null) return;

            plusOneText.GetComponent<Text>().DOFade(1f, 0.1f);
            plusOneText.transform.DOLocalMoveZ(plusOneText.transform.localPosition.z, 0.5f)
                .OnComplete(() =>
                {
                    plusOneText.GetComponent<Text>().DOFade(0f, 0.2f);
                });
        }
    }

    public void BetrayGroup()
    {
        CurrentHumanState = HumanState.Judas;
        //foreach (MeshRenderer mr in screenMRs)
        //{
        //    mr.material.SetColor("_Color", Color.black);
        //}
        //foreach (MeshRenderer mr in screenMRs)
        //{
        //    mr.material.SetColor("_Skin", Color.gray);
        //}
        velocity = Vector3.zero;
        _followTargetDisposable?.Dispose();
        var ground = FindObjectsOfType<LevelGenerator>()[0];
        transform.parent = ground.transform;

        if (GroupController.Instance.HumanList.Contains(this))
            GroupController.Instance.HumanList.Remove(this);

        GroupController.Instance.CheckHumansAmount();
    }
}
public enum HumanState
{
    Recruit,
    Follower,
    Judas
}
