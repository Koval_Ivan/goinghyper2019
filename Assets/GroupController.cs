﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GroupController : MonoBehaviour
{
    public static GroupController Instance;

    public Transform FollowTarget;
    public Camera MainCamera;
    private Plane _plane;

    public List<Human> HumanList;
    public GameObject HumanPrefab;

    public float CircleDelta = 0;
    public float SpawnPosRandomizer = 3f;
    public float ScreenHeightBound = 0.5f;

    public float TapThreshold = 0.1f;
    public float TapDuration = 0f;

    private void Awake()
    {
        Instance = this;
        InitGroup();
    }

    private void Start()
    {
        _plane = new Plane(Vector3.forward, new Vector3(0, 0, 0));

        this.UpdateAsObservable()
            .Where(x => Input.GetMouseButtonDown(0))
            .Subscribe(x => { FollowTarget.DOKill(); });

        this.UpdateAsObservable()
            .Where(x => Input.GetMouseButton(0)) 
            .Subscribe(x =>
            {
                TapDuration += Time.smoothDeltaTime;

                if (TapDuration <= TapThreshold) return;

                var mousePos = Input.mousePosition;
                mousePos.y = mousePos.y > Screen.height * ScreenHeightBound
                    ? Screen.height * ScreenHeightBound
                    : mousePos.y;
                FollowTarget.position = GetWorldPositionOnPlane(mousePos) + new Vector3(0f, CircleDelta, 0f);
            });

        this.UpdateAsObservable()
            .Where(x => Input.GetMouseButtonUp(0))
            .Subscribe(x =>
            {
                FollowTarget.DOMove(Vector3.down * 4f, 2f);
                TapDuration = 0f;
            });
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition)
    {
        Ray ray = MainCamera.ScreenPointToRay(screenPosition);
        _plane.Raycast(ray, out var distance);
        return ray.GetPoint(distance);
    }

    private void InitGroup()
    {
        HumanList = new List<Human>();

        var humansToSpawn = PlayerPrefs.GetInt(Constants.PlayerPrefs.LastRoundSurvivedAmount, 1);

        for (int i = 0; i < humansToSpawn; i++)
        {
            var initialOffset = i != 0 ? GetRandomOffset() : Vector3.zero;
            var spawnPos = FollowTarget.position + initialOffset;
            var newHuman = Instantiate(HumanPrefab, spawnPos, HumanPrefab.transform.rotation).GetComponent<Human>();
            //HumanList.Add(newHuman);
            newHuman.InitialOffset = initialOffset;
            newHuman.InitAsFollower();
        }
    }

    public Vector3 GetOffset(Vector3 recruitPos)
    {
        return recruitPos - FollowTarget.position;
    }

    public Vector3 GetRandomOffset()
    {
        return Vector3.Scale(Random.insideUnitCircle * SpawnPosRandomizer, new Vector3(1f, 0.5f, 0f));
    }

    public void CheckHumansAmount()
    {
        if (HumanList.Count == 0)
        {
            Observable.Timer(TimeSpan.FromSeconds(2f))
                .Subscribe(x => { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); });
        }
    }

    public void SetHumansFollow(bool follow)
    {
        foreach (var human in HumanList)
        {
            human.FollowTarget = follow;
        }
    }

    public void RecalculateBounds()
    {
        var newBoundsValue = Mathf.Clamp(Mathf.Sqrt(HumanList.Count) * 0.66f, 0f, 4f);

        foreach (var human in HumanList)
        {
            human.bounds = newBoundsValue;
        }
    }
}
