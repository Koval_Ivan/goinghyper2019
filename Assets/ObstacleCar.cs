﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class ObstacleCar : MonoBehaviour
{
    public float MoveSpeed = 5f;
    public float border = 20f;

    private void Start()
    {

        this.UpdateAsObservable()
            .Subscribe(x =>
            {
                transform.Translate(Vector3.up * MoveSpeed * Time.smoothDeltaTime);

                if (transform.localPosition.x > border || transform.localPosition.x<-border)
                {
                    transform.position -= transform.up * 2f * border;
                }
            });
    }
}
