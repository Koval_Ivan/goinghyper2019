﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public static class PlayerPrefs
    {
        public static string LastRoundSurvivedAmount = "LastRoundSurvivedAmount";
    }

    public static class Tags
    {
        public static string Human = "Human";
        public static string Obstacle = "Obstacle";
        public static string ObstacleCar = "ObstacleCar";
    }
}
