﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class Subway : MonoBehaviour
{
    public Transform Door11;
    public Transform Door12;
    public Transform Door21;
    public Transform Door22;

    public GameObject FrontDoor;

    private IDisposable _trigger;

    private bool trigone = false;

    private void Start()
    {
        _trigger = this.OnTriggerEnter2DAsObservable()
            .Subscribe(x =>
            {
                if (x.tag == Constants.Tags.Human)
                    x.gameObject.GetComponent<Human>().IsInSubway = true;

                //GameController.Instance._currentLevelGenerator.isActive = false;

                //Observable.Timer(TimeSpan.FromSeconds(0.2f))
                //    .Subscribe(y =>
                //    {
                //        x.GetComponent<Human>().FollowTarget = false;
                //    });

                //if (GroupController.Instance.HumanList.TrueForAll(human => human.IsInSubway))
                //{
                //_trigger?.Dispose();

                if (!trigone)
                {
                    Observable.Timer(TimeSpan.FromSeconds(0.2f))
                        .Subscribe(y =>
                        {
                            GroupController.Instance.SetHumansFollow(false);
                            CloseInDoors();

                            //foreach (var human in GroupController.Instance.HumanList)
                            //{

                            //}
                        });

                    trigone = true;
                }

                //}
            });
    }

    public void CloseInDoors()
    {
        GameController.Instance._currentLevelGenerator.isActive = false;


        Door11.DOLocalMoveZ(-6.7f, 0.5f).SetDelay(1f);
        Door12.DOLocalMoveZ(-6.7f, 0.5f).SetDelay(1f)
            .OnComplete(() =>
            {
                GameController.Instance.OnMetroReached(this);

                for (int i = GroupController.Instance.HumanList.Count - 1; i >= 0; i--)
                {
                    if (!GroupController.Instance.HumanList[i].IsInSubway)
                    {
                        GroupController.Instance.HumanList[i].BetrayGroup();
                    }
                }

            });
    }

    public void OpenOutDoors(Action onComplete)
    {
        FrontDoor.SetActive(true);

        foreach (var collider in GetComponentsInChildren<Collider2D>())
        {
            collider.enabled = false;
        }

        foreach (var collider in transform.parent.GetComponentsInChildren<Collider2D>())
        {
            collider.enabled = false;
        }

        Door21.DOLocalMoveZ(-9.82f, 0.5f);
        Door22.DOLocalMoveZ(-3.53f, 0.5f)
            .OnComplete(() =>
            {
                onComplete?.Invoke();

                Observable.Timer(TimeSpan.FromSeconds(2f))
                    .Subscribe(y =>
                    {
                        Destroy(transform.parent.gameObject);
                    });
            });
    }
}
