﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    public List<GameObject> Levels;
    public float LastGeneratorPositionY = 0f;
    public float XDeltaBetweenGenerators = 50f;
    public int CurrentLevel = 0;
    public LevelGenerator _currentLevelGenerator;
    public Subway CurrentSubway;

    public GameObject tapText;
    public Image tapImage;

    private void Awake()
    {
        Instance = this;

        Observable.Timer(TimeSpan.FromSeconds(0.5f))
            .Subscribe(y =>
            {
                Time.timeScale = 0f;
                tapImage.DOFade(0f, 0.5f)
                    .OnComplete(() =>
                    {
                        StartCoroutine(WaitForTap());
                    });
            });
    }

    private void Start()
    {
        SpawnGenerator();
    }

    private IEnumerator WaitForTap()
    {
        yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
        tapText.SetActive(false);

        Time.timeScale = 1f;
    }

    public void SpawnGenerator(bool makeOffset = false)
    {
        var newLevelGen = Instantiate(Levels[CurrentLevel].gameObject, 
                makeOffset ? Vector3.left * XDeltaBetweenGenerators : Vector3.zero, Quaternion.identity, transform)
            .GetComponent<LevelGenerator>();

        CurrentLevel++;

        if (CurrentLevel == 3)
        {
            CurrentLevel = 0;
        }

        newLevelGen.InitLevelGenerator(LastGeneratorPositionY);
        _currentLevelGenerator = newLevelGen;
    }

    public void OnMetroReached(Subway currentSubway)
    {
        CurrentSubway = currentSubway;
        var activeMetro = GameObject.Find("Subway");
        activeMetro.transform.parent = null;

        var oldLevelGenerator = _currentLevelGenerator.gameObject;
        _currentLevelGenerator.gameObject.transform.DOMoveX(XDeltaBetweenGenerators, 4f).SetEase(Ease.InOutCubic);

        _currentLevelGenerator.StopMove();

        LastGeneratorPositionY += (_currentLevelGenerator.transform.position.y + _currentLevelGenerator.Offset.y);

        //Debug.Log("Last: " +LastGeneratorPositionY);

        SpawnGenerator(true);
        _currentLevelGenerator.isActive = false;

        _currentLevelGenerator.gameObject.transform.DOMoveX(0f, 4.25f).SetEase(Ease.InOutCubic)
            .OnComplete(() =>
            {
                CurrentSubway.OpenOutDoors(() =>
                {
                    GroupController.Instance.SetHumansFollow(true);
                    _currentLevelGenerator.isActive = true;
                    activeMetro.transform.parent = _currentLevelGenerator.transform;
                    Destroy(oldLevelGenerator);
                });
            });
    }
}
