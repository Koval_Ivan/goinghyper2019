﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] List<Segment> segments;
    [SerializeField] public float speed = 6f;

    public bool isActive = true;
    public Vector3 Offset;

    private IDisposable _moveDisposable;

    public LevelGenerator InitLevelGenerator(float initialOffsetY)
    {
        Debug.Log(initialOffsetY);
        for (var i = 0; i < segments.Count; i++)
        {
            var newSegment = Instantiate(segments[i], transform.position + Offset + new Vector3(0f, initialOffsetY, 0f),
                Quaternion.identity, transform);

            Offset += segments[i].length * Vector3.up;
        }

        _moveDisposable = this.UpdateAsObservable()
            .Subscribe(x =>
            {
                if (Input.GetMouseButtonDown(1)) isActive = !isActive;

                if (!isActive) return;

                transform.position += Vector3.down * speed * Time.deltaTime;
            });

        return this;
    }

    public void StopMove()
    {
        _moveDisposable?.Dispose();
    }
}
